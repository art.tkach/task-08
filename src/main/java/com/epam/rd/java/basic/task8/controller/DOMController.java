package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flover;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;

import java.util.HashSet;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private DocumentBuilder db;
	private final HashSet<Flover> flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new HashSet<>();
		try{
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		}catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

	}


	public void parceDOM() throws ParserConfigurationException, IOException, SAXException {
		Document doc = db.parse(xmlFileName);
		Element root = doc.getDocumentElement();
		NodeList floverList = root.getElementsByTagName("flower");
		for (int i = 0; i < floverList.getLength(); i++) {
			Element floverElement = (Element) floverList.item(i);
			Flover flover = buildFlower(floverElement);
			flowers.add(flover);
		}
}

	private Flover buildFlower(Element flowerElement) {
		Flover flower = new Flover();
		flower.setName(getElementTextContent(flowerElement, "name"));
		flower.setSoil(getElementTextContent(flowerElement, "soil"));
		flower.setOrigin(getElementTextContent(flowerElement, "origin"));
		flower.setStemColour(getElementTextContent(flowerElement, "stemColour"));
		flower.setLeafColour(getElementTextContent(flowerElement, "leafColour"));
		flower.setAveLenFlower(Integer.parseInt(getElementTextContent(flowerElement, "aveLenFlower")));
		flower.setTempreture(Integer.parseInt(getElementTextContent(flowerElement, "tempreture")));
		flower.setLighting(getAttributeOfElement(flowerElement,"lighting","lightRequiring"));
		flower.setWatering(Integer.parseInt(getElementTextContent(flowerElement, "watering")));
		flower.setMultiplying(getElementTextContent(flowerElement, "multiplying"));
		flower.setAveLenFlowerMeasure(getAttributeOfElement(flowerElement,"aveLenFlower","measure"));
		flower.setTempretureMeasure(getAttributeOfElement(flowerElement,"tempreture","measure"));
		flower.setWateringMeasure(getAttributeOfElement(flowerElement,"watering","measure"));

		return flower;
	}

	// get the text content of the tag
	private static String getElementTextContent(Element element, String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}

	// get the value content of the tag
	private static String getAttributeOfElement(Element element, String elementName, String attributeName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		NamedNodeMap nm = node.getAttributes();
		Node att = nm.getNamedItem(attributeName);
		return att.getNodeValue();
	}

	public void transform(String outPutFile) throws ParserConfigurationException {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();
		Element root = document.createElement("flowers");
		root.setAttribute("xmlns","http://www.nure.ua");
		root.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");
		document.appendChild(root);
		for (Flover flower:flowers) {
			addFlowerToXML(document, root, flower);
		}
		// write tree to file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new FileWriter(outPutFile));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes" );
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public final void printElements(){
		for (Flover f: flowers){
			System.out.println(f.toString());
		}
	}

	private void addFlowerToXML(Document document, Element rootElement, Flover flower){
		Element tempElement;
		Element elementFlower = document.createElement("flower");
		rootElement.appendChild(elementFlower);
		//Name
		tempElement = document.createElement("name");
		tempElement.appendChild(document.createTextNode(flower.getName()));
		elementFlower.appendChild(tempElement);
		//Soil
		tempElement = document.createElement("soil");
		tempElement.appendChild(document.createTextNode(flower.getSoil()));
		elementFlower.appendChild(tempElement);
		//origin
		tempElement = document.createElement("origin");
		tempElement.appendChild(document.createTextNode(flower.getOrigin()));
		elementFlower.appendChild(tempElement);
		//region visualParameters
		Element elementVisualParameters = document.createElement("visualParameters");
		elementFlower.appendChild(elementVisualParameters);
		//stem Colour
		tempElement = document.createElement("stemColour");
		tempElement.appendChild(document.createTextNode(flower.getStemColour()));
		elementVisualParameters.appendChild(tempElement);
		//leaf Colour
		tempElement = document.createElement("leafColour");
		tempElement.appendChild(document.createTextNode(flower.getLeafColour()));
		elementVisualParameters.appendChild(tempElement);
		//endRegion

		//region growingTips
		Element elementGrowingTips = document.createElement("growingTips");
		elementFlower.appendChild(elementGrowingTips);
		//temperature
		tempElement = document.createElement("tempreture");
		tempElement.appendChild(document.createTextNode(Integer.toString(flower.getTempreture())));
		tempElement.setAttribute("measure", flower.getTempretureMeasure());
		elementGrowingTips.appendChild(tempElement);
		//lighting
		tempElement = document.createElement("lighting");
		tempElement.setAttribute("lightRequiring", flower.getLighting());
		elementGrowingTips.appendChild(tempElement);
		//watering
		tempElement = document.createElement("watering");
		tempElement.appendChild(document.createTextNode(Integer.toString(flower.getWatering())));
		tempElement.setAttribute("measure", flower.getWateringMeasure());
		elementGrowingTips.appendChild(tempElement);
		//endRegion

		//multiplying
		tempElement = document.createElement("multiplying");
		tempElement.appendChild(document.createTextNode(flower.getMultiplying()));
		elementFlower.appendChild(tempElement);
	}

}
