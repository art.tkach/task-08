package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flover;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Stack;

public class FlowerHandler extends DefaultHandler {
    private final ArrayList<Flover> flowers;
    private Flover currentFlower;
    private static final String ELEMENT_FLOWER = "flower";
    private Stack<String> elementStack;

    public FlowerHandler() {
        flowers = new ArrayList<>();
        elementStack = new Stack();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementStack.push(qName);

        if (ELEMENT_FLOWER.equals(qName)){
            currentFlower = new Flover();
            flowers.add(currentFlower);
            return;
        }
        switch (qName){
            case "aveLenFlower": currentFlower.setAveLenFlowerMeasure(attributes.getValue(0)); break;
            case "tempreture": currentFlower.setTempretureMeasure(attributes.getValue(0)); break;
            case "lighting": currentFlower.setLighting(attributes.getValue(0)); break;
            case "watering": currentFlower.setWateringMeasure(attributes.getValue(0)); break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementStack.pop();
    }
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();
        if (value.length() == 0) return; // ignore white space
        //handle the value based on to which element it belongs
        switch (currentElement()){
            case ("name"):
                currentFlower.setName(value);
                break;
            case ("soil"):
                currentFlower.setSoil(value);
                break;
            case ("origin"):
                currentFlower.setOrigin(value);
                break;
            case ("stemColour"):
                currentFlower.setStemColour(value);
                break;
            case ("leafColour"):
                currentFlower.setLeafColour(value);
                break;
            case ("aveLenFlower"):
                currentFlower.setAveLenFlower(Integer.parseInt(value));
                break;
            case ("tempreture"):
                currentFlower.setTempreture(Integer.parseInt(value));
                break;
            case ("watering"):
                currentFlower.setWatering(Integer.parseInt(value));
                break;
            case ("multiplying"):
                currentFlower.setMultiplying(value);
                break;
        }
    }
    private String currentElement()
    {
        return (String) elementStack.peek();
    }
    public ArrayList<Flover> getFlowers(){
        return flowers;
    }
}
