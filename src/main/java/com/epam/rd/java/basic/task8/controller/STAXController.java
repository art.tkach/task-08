package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flover;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	ArrayList<Flover> flowers;
	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new ArrayList<>();
	}

	public void readXML() {
		Flover flower = null;
		Attribute attribute = null;
		final String MEASURE_TAG_NAME = "measure";

		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try(FileInputStream fos = new FileInputStream(xmlFileName)){
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(fos);
			while (reader.hasNext()){
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "flower":
							flower = new Flover();
							break;
						case "name":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setName(nextEvent.asCharacters().getData());
							break;
						case "soil":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setSoil(nextEvent.asCharacters().getData());
							break;
						case "origin":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setOrigin(nextEvent.asCharacters().getData());
							break;
						case "stemColour":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setStemColour(nextEvent.asCharacters().getData());
							break;
						case "leafColour":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setLeafColour(nextEvent.asCharacters().getData());
							break;
						case "aveLenFlower":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
							attribute = startElement.getAttributeByName(new QName(MEASURE_TAG_NAME));
							//Добавляю атрибут
							if (attribute!=null&&flower!=null) flower.setAveLenFlowerMeasure(attribute.getValue());
							break;
						case "tempreture":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setTempreture(Integer.parseInt(nextEvent.asCharacters().getData()));
							attribute = startElement.getAttributeByName(new QName(MEASURE_TAG_NAME));
							//Читаю атрибут
							if (attribute!=null&&flower!=null) flower.setTempretureMeasure(attribute.getValue());
							break;
						case "lighting":
							nextEvent = reader.nextEvent();
							attribute = startElement.getAttributeByName(new QName("lightRequiring"));
							//adding attribute
							if (attribute!=null&&flower!=null) flower.setLighting(attribute.getValue());
							break;
						case "watering":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
							attribute = startElement.getAttributeByName(new QName(MEASURE_TAG_NAME));
							//adding attribute
							if (attribute!=null&&flower!=null) flower.setWateringMeasure(attribute.getValue());
							break;
						case "multiplying":
							nextEvent = reader.nextEvent();
							if (flower!=null) flower.setMultiplying(nextEvent.asCharacters().getData());
							break;
					}
				}
		if (nextEvent.isEndElement()) {
			EndElement endElement = nextEvent.asEndElement();
			if (endElement.getName().getLocalPart().equals("flower")) {
				flowers.add(flower);
			}
		}
			}
		} catch (XMLStreamException|IOException e) {
			e.printStackTrace();
		}
	}

	public void writeXML(String outputXmlFile){
		//try(FileOutputStream out = new FileOutputStream(outputXmlFile)){
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();){
			createXML(out);

			String xml = new String(out.toByteArray(), StandardCharsets.UTF_8);
			String prettyPrintXML = formatXML(xml);
			Files.writeString(Paths.get(outputXmlFile), prettyPrintXML, StandardCharsets.UTF_8);

		} catch (IOException|XMLStreamException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	private void createXML(ByteArrayOutputStream out) throws XMLStreamException {
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = output.createXMLStreamWriter(out);
		writer.writeStartDocument("utf-8", "1.0");
		writer.writeStartElement("flowers");
		writer.writeAttribute("xmlns", "http://www.nure.ua");
		writer.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");

		for (Flover f:flowers) {
			writer.writeStartElement("flower");
			writeElement(writer,"name",f.getName());
			writeElement(writer,"soil",f.getSoil());
			writeElement(writer,"origin",f.getOrigin());

			writer.writeStartElement("visualParameters");
			writeElement(writer, "stemColour", f.getStemColour());
			writeElement(writer, "leafColour", f.getLeafColour());
			writeElementWithAttribute(writer,"aveLenFlower",Integer.toString(f.getAveLenFlower()),"measure", f.getAveLenFlowerMeasure());
			writer.writeEndElement();//visualParameters

			writer.writeStartElement("growingTips");
			writeElementWithAttribute(writer,"tempreture",Integer.toString(f.getTempreture()),"measure", f.getTempretureMeasure());
			writeElementWithAttribute(writer,"lighting",null,"lightRequiring", f.getLighting());
			writeElementWithAttribute(writer,"watering",Integer.toString(f.getWatering()),"measure", f.getWateringMeasure());
			writer.writeEndElement();//growingTips

			writeElement(writer,"multiplying",f.getMultiplying());

			writer.writeEndElement();//flower
		}
		writer.writeEndElement();//flowers
		writer.writeEndDocument();//root
		//
		writer.flush();
		writer.close();
	}

	public final void printElements(){
		for (Flover f: flowers){
			if (f==null) continue;
			System.out.println(f.toString());
		}
	}
	public void sortFlowersByLength(){
		flowers.sort(new Comparator<Flover>() {
			@Override
			public int compare(Flover o1, Flover o2) {
				return o1.getName().length() - o2.getName().length();
			}
		});
	}

	private void writeElement(XMLStreamWriter writer, String elementName, String value) throws XMLStreamException {
		writer.writeStartElement(elementName);
		writer.writeCharacters(value);
		writer.writeEndElement();
	}

	private void writeElementWithAttribute(XMLStreamWriter writer, String elementName, String value, String attName, String attValue) throws XMLStreamException {
		writer.writeStartElement(elementName);
		writer.writeAttribute(attName, attValue);
		if (value!=null) writer.writeCharacters(value);
		writer.writeEndElement();
	}

	private static String formatXML(String xml) throws TransformerException {
		// write data to xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		// pretty print by indention
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		// add standalone="yes", add line break before the root element
		transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
		StreamSource source = new StreamSource(new StringReader(xml));
		StringWriter output = new StringWriter();
		transformer.transform(source, new StreamResult(output));

		return output.toString();
	}
}
