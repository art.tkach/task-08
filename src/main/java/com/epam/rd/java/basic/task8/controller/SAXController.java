package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flover;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Handler;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	ArrayList<Flover> flowers;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new ArrayList<>();
	}

	// PLACE YOUR CODE HERE
	public void readXML(){
		try {
			// SAX parser creating & configuring
			SAXParserFactory factory = SAXParserFactory.newInstance();
			FlowerHandler handler = new FlowerHandler();
			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();
			reader.setContentHandler(handler);
			reader.setErrorHandler(new FlowerErrorHandler());
			reader.parse(xmlFileName);
			flowers = handler.getFlowers();

		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public final void printElements(){
		for (Flover f: flowers){
			System.out.println(f.toString());
		}
	}

	public void writeXML(String outputXmlFile){
		//try(FileOutputStream out = new FileOutputStream(outputXmlFile)){
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();){
			createXML(out);

			String xml = new String(out.toByteArray(), StandardCharsets.UTF_8);
			String prettyPrintXML = formatXML(xml);
			Files.writeString(Paths.get(outputXmlFile), prettyPrintXML, StandardCharsets.UTF_8);

		} catch (IOException| XMLStreamException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	private void createXML(ByteArrayOutputStream out) throws XMLStreamException {
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = output.createXMLStreamWriter(out);
		writer.writeStartDocument("utf-8", "1.0");
		writer.writeStartElement("flowers");
		writer.writeAttribute("xmlns", "http://www.nure.ua");
		writer.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");

		for (Flover f:flowers) {
			writer.writeStartElement("flower");
			writeElement(writer,"name",f.getName());
			writeElement(writer,"soil",f.getSoil());
			writeElement(writer,"origin",f.getOrigin());

			writer.writeStartElement("visualParameters");
			writeElement(writer, "stemColour", f.getStemColour());
			writeElement(writer, "leafColour", f.getLeafColour());
			writeElementWithAttribute(writer,"aveLenFlower",Integer.toString(f.getAveLenFlower()),"measure", f.getAveLenFlowerMeasure());
			writer.writeEndElement();//visualParameters

			writer.writeStartElement("growingTips");
			writeElementWithAttribute(writer,"tempreture",Integer.toString(f.getTempreture()),"measure", f.getTempretureMeasure());
			writeElementWithAttribute(writer,"lighting",null,"lightRequiring", f.getLighting());
			writeElementWithAttribute(writer,"watering",Integer.toString(f.getWatering()),"measure", f.getWateringMeasure());
			writer.writeEndElement();//growingTips

			writeElement(writer,"multiplying",f.getMultiplying());

			writer.writeEndElement();//flower
		}
		writer.writeEndElement();//flowers
		writer.writeEndDocument();//root
		//
		writer.flush();
		writer.close();
	}
	private void writeElement(XMLStreamWriter writer, String elementName, String value) throws XMLStreamException {
		writer.writeStartElement(elementName);
		writer.writeCharacters(value);
		writer.writeEndElement();
	}

	private void writeElementWithAttribute(XMLStreamWriter writer, String elementName, String value, String attName, String attValue) throws XMLStreamException {
		writer.writeStartElement(elementName);
		writer.writeAttribute(attName, attValue);
		if (value!=null) writer.writeCharacters(value);
		writer.writeEndElement();
	}

	private static String formatXML(String xml) throws TransformerException {
		// write data to xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		// pretty print by indention
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		// add standalone="yes", add line break before the root element
		transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
		StreamSource source = new StreamSource(new StringReader(xml));
		StringWriter output = new StringWriter();
		transformer.transform(source, new StreamResult(output));

		return output.toString();
	}
	public void sortFlowersByTempreture(){
		flowers.sort(new Comparator<Flover>() {
			@Override
			public int compare(Flover o1, Flover o2) {
				return o1.getTempreture() - o2.getTempreture();
			}
		});
	}
}