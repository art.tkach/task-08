package com.epam.rd.java.basic.task8;

public class Flover {

    private String name;
    private String soil;
    private String origin;
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private int tempreture;
    private boolean lighting;
    private int watering;
    private String multiplying;
    private String aveLenFlowerMeasure;
    private String tempretureMeasure;
    private String wateringMeasure;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighting() {
        return (lighting)?"yes":"no";
    }

    public void setLighting(String lighting) {
        this.lighting = lighting.equals("yes");
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }

    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    public String getTempretureMeasure() {
        return tempretureMeasure;
    }

    public void setTempretureMeasure(String tempretureMeasure) {
        this.tempretureMeasure = tempretureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    @Override
    public String toString() {
        return "Flover{" +
                "name='" + name + '\'' +
                "stemColour='" + getStemColour() + "'}";
    }
}
